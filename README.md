# Prueba_tecnica_evertec

### PASOS PARA CONSTRUIR LA APP
> Este proyecto esta construido de forma modular, por lo tanto, maneja varios modulos
### Modulo Domain e Infrastructure
> demos ingresar a la ruta de los modulo, y ejecutar el siguiente comando en nuestra terminal.
#### dart pub get
#### dart run build_runner build
> Este comando permite generar codigo necesario, el cual se usa para injectar las clases. 


#### API_KEY
> Demos de agregar en el modulo infrastructure, nuestra api_key
en el archivo .ENV


### Modulo principal (UI APP)
> demos estar en la ruta principal, y ejecutar el siguiente comando en nuestra terminal.
#### Flutter pub get
####  flutter packages pub run build_runner build

## Una vez hecho estos pasos ya podriamos ejecutar nuestra App MOVIE.
