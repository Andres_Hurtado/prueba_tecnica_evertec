import 'package:domain/src/movie/model/movie.dart';
import 'package:domain/src/movie/repository/movie_repository.dart';
import 'package:domain/src/movie/services/movie_services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../test_data_builder/test_data_base_builder_movie.dart';
import '../test_data_builder/test_data_base_builder_movie_simplified.dart';
import '../test_data_builder/test_data_builder_movie.dart';
import '../test_data_builder/test_data_builder_movie_simplified.dart';

class MockMovieRepository extends Mock implements MovieRepository {}

void main() {
  final builderSimplified = MovieSimplifiedBuilder()
    ..id = 130
    ..originalTitle = ''
    ..posterPath = 'imagen'
    ..releaseDate = DateTime.now()
    ..title = '';

  final movieSimplified = BaseBuiderMovieSimplified(builderSimplified).build();

  final builder = MovieBuilder()
    ..id = 130
    ..originalTitle = ''
    ..overview = ''
    ..popularity = 0
    ..posterPath = 'imagen'
    ..releaseDate = DateTime.now()
    ..title = ''
    ..genres = [];
  final movie = BaseBuiderMovie(builder).build();

  late MovieRepository movieRepository;
  late MovieService service;

  setUp(() {
    movieRepository = MockMovieRepository();
    service = MovieService(movieRepository: movieRepository);
    // registerFallbackValue([movieSimplified]);
    // registerFallbackValue(movie);
  });

  group('Movie Service', () {
    // test('constructor', () async {
    //   //Arrange
    //   const page = 1;
    //   final builderSimplified = MovieSimplifiedBuilder()
    //     ..id = 130
    //     ..adult = false
    //     ..originalLanguage = OriginalLanguage.EN
    //     ..originalTitle = ''
    //     ..overview = ''
    //     ..popularity = 0
    //     ..posterPath = ''
    //     ..releaseDate = DateTime.now()
    //     ..title = ''
    //     ..voteAverage = 0
    //     ..voteCount = 0;
    //   final movieSimplifiedNoPosterPath =
    //       BaseBuiderMovieSimplified(builderSimplified).build();
    //   when(() async => service.getMovies(page))
    //       .thenAnswer((_) async => [movieSimplifiedNoPosterPath]);
    //   // when(() async => service.getMovies(page))
    //   //     .thenThrow((_) => PosterPathIsEmptyException());
    //   //Act
    //   //Assert
    //   expect(
    //     () => service.getMovies(page),
    //     throwsA(isInstanceOf<PosterPathIsEmptyException>()),
    //   );
    //   // verify(() => service.getMovies(page)).called(1);
    // });

    test('movieService_whenIsInvokedGetMovies_success', () async {
      //Arrange
      const page = 1;
      const generatedAmountMovie = 10;
      final moviesResponse =
          List.generate(generatedAmountMovie, (_) => movieSimplified);
      when(() => service.getMovies(any()))
          .thenAnswer((_) async => moviesResponse);
      //Act
      final movies = await service.getMovies(page);
      //Assert
      verify(() => service.getMovies(page)).called(1);
      expect(moviesResponse, movies);
    });

    test('movieService_whenIsInvokedGetMovie_success', () async {
      //Arrange
      const id = 130;
      when(() => service.getMovie(any())).thenAnswer((_) async => movie);
      //Act
      final dataMovie = await service.getMovie(id);
      //Assert
      verify(() => service.getMovie(id)).called(1);
      expect(dataMovie, isA<Movie>());
    });
  });
}
