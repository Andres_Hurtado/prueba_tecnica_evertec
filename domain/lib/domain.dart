import 'package:injectable/injectable.dart';

export 'package:domain/src/movie/model/movie.dart' show Movie;
export 'package:domain/src/movie/model/movie_simplified.dart' show MovieSimplified;
export 'package:domain/src/movie/repository/movie_repository.dart' show MovieRepository;
export 'package:domain/src/movie/services/movie_services.dart' show MovieService; 


//* Config para la injection, de forma modular.
@InjectableInit.microPackage()
void initDomain() {}
