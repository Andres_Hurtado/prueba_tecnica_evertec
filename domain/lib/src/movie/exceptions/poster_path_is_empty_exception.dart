const String _message = 'Obtuvimos un error, a la hora de obtener la imagen.';

class IsEmptyException implements Exception {
  const IsEmptyException({
    this.message = _message,
  });
  final String message;
}
