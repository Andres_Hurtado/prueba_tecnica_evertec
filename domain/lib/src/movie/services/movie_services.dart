import 'package:domain/src/movie/model/movie.dart';
import 'package:domain/src/movie/model/movie_simplified.dart';
import 'package:domain/src/movie/repository/movie_repository.dart';
import 'package:injectable/injectable.dart';

@injectable
class MovieService {
  MovieService({required MovieRepository movieRepository})
      : _movieRepository = movieRepository;

  final MovieRepository _movieRepository;

  Future<List<MovieSimplified>> getMovies(int page) {
    return _movieRepository.get(page);
  }

  Future<Movie> getMovie(int id) {
    return _movieRepository.getById(id);
  }

  Future<List<MovieSimplified>> searchMovie(
    int page, {
    required String textQuery,
  }) {
    return _movieRepository.searchMovie(page, textQuery: textQuery);
  }
}
