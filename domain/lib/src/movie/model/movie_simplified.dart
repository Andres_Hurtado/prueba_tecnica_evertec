import 'package:domain/src/movie/exceptions/poster_path_is_empty_exception.dart';

class MovieSimplified {
  MovieSimplified({
    required this.id,
    required this.originalTitle,
    required this.posterPath,
    required this.releaseDate,
    required this.title,
  }) {
    if (isEmpty()) {
      throw const IsEmptyException();
    }
  }

  final int id;
  final String originalTitle;
  //* Puede venir vacio.
  final String posterPath;
  final DateTime releaseDate;
  final String title;

  bool isEmpty() => originalTitle.isEmpty && title.isEmpty;
}
