import 'package:domain/src/movie/model/movie_simplified.dart';

class Movie extends MovieSimplified {
  Movie({
    required super.id,
    required super.title,
    required super.posterPath,
    required this.popularity,
    required super.releaseDate,
    required this.overview,
    required super.originalTitle,
    required this.genres,
    required this.createdAt,
    required this.updatedAt,
  });
  final List<String> genres;
  final String overview;
  final double popularity;
  final DateTime createdAt;
  final DateTime updatedAt;
}
