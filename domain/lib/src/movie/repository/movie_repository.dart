import 'package:domain/domain.dart';

abstract interface class MovieRepository {
  Future<List<MovieSimplified>> get(int page);
  Future<Movie> getById(int id);
  Future<List<MovieSimplified>> searchMovie(
    int page, {
    required String textQuery,
  });
}
