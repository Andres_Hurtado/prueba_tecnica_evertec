part of 'movie_bloc.dart';

sealed class MovieState extends Equatable {
  const MovieState();
  @override
  List<Object> get props => [];
}

final class MovieInitial extends MovieState {}

class MovieLoading extends MovieState {
  const MovieLoading(this.oldMovies, {required this.isFirstFetch});

  final List<MovieSimplified> oldMovies;
  final bool isFirstFetch;

  @override
  List<Object> get props => [oldMovies, isFirstFetch];
}

class MovieLoaded extends MovieState {
  const MovieLoaded(
    this.movies, {
    this.complete = false,
    this.withoutResult = false,
  });

  final List<MovieSimplified> movies;
  final bool complete;
  final bool withoutResult;

  @override
  List<Object> get props => [movies, complete, withoutResult];
}

class MovieError extends MovieState {
  const MovieError(this.message);
  final String message;
  @override
  List<Object> get props => [message];
}
