import 'package:bloc/bloc.dart';
import 'package:domain/domain.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:infrastructure/infrastructure.dart';
import 'package:injectable/injectable.dart';
import 'package:movie/src/dependencyInjection/dependency_injection.dart';
part 'movie_event.dart';
part 'movie_state.dart';

@LazySingleton()
class MovieBloc extends Bloc<MovieEvent, MovieState> {
  MovieBloc({required MovieService movieService})
      : _movieService = movieService,
        super(MovieInitial()) {
    on<GetMovies>(_onGetMoviesEvent);
  }

  final MovieService _movieService;

  int page = 1;

  Future<void> _onGetMoviesEvent(
    GetMovies event,
    Emitter<MovieState> emit,
  ) async {
    //* Nos permite resetear el state,
    //* ya que hay un cambio de tipo animal.
    if (event.resetThereIsNewPagination) {
      page = 1;
      emit(MovieInitial());
    }

    if (state is MovieLoading) return;
    if (state is MovieLoaded && (state as MovieLoaded).complete) {
      return;
    }

    final currentState = state;
    var oldMovies = <MovieSimplified>[];

    if (currentState is MovieLoaded) {
      oldMovies = currentState.movies;
    }

    emit(MovieLoading(oldMovies, isFirstFetch: page == 1));

    try {
      final newMovies = await _movieService.getMovies(page);

      //* Validamos que la consulta nos traiga datos,
      //* en caso de que sea vacio,
      //* damos por hecho de que ya no hay mas datos,
      //* por lo tanto notificamos con la vaible complete.
      if (newMovies.isEmpty) {
        final oldMovies = (state as MovieLoading).oldMovies;
        final isFirstFetch = (state as MovieLoading).isFirstFetch;

        if (!isFirstFetch) {
          emit(MovieLoaded(oldMovies, complete: true));
          return;
        } else {
          emit(MovieLoaded(oldMovies, withoutResult: true));
          return;
        }
      } else {
        //* Aumentamos la variable page, para futura, paginacion.
        page++;
      }

      //* Obtenemos los datos guardados de las peliculas,
      //* junto a su vez tenemos las nuevas peliculas ya que se los insertamos.
      final oldMovies = (state as MovieLoading).oldMovies..addAll(newMovies);

      //* Cambiamos de estado con todos los datos.
      emit(MovieLoaded(oldMovies));
    } on NotConnectedToNetworkException catch (e) {
      emit(MovieError(e.message));
    } on RequiredKeyException catch (e) {
      emit(MovieError(e.message));
    } on HttpClientException catch (e) {
      emit(MovieError(e.message));
    } catch (e) {
      final message = e.toString();
      emit(MovieError(message));
    }
  }

  @override
  Future<void> close() {
    debugPrint('MovieBloc - close $_movieService');
    if (getIt.isRegistered<MovieBloc>(instance: this)) {
      getIt.resetLazySingleton<MovieBloc>(instance: this);
    }
    return super.close();
  }
}
