part of 'movie_bloc.dart';

sealed class MovieEvent extends Equatable {
  const MovieEvent();
  @override
  List<Object> get props => [];
}

class GetMovies extends MovieEvent {
  const GetMovies({this.resetThereIsNewPagination = false});
  final bool resetThereIsNewPagination;
  @override
  List<Object> get props => [resetThereIsNewPagination];
}
