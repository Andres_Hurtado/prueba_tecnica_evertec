import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie/generated/l10n.dart';
import 'package:movie/src/screens/detail/detail_screen.dart';
import 'package:movie/src/screens/home/bloc/home/movie_bloc.dart';
import 'package:movie/src/screens/home/contants/home_contants.dart';
import 'package:movie/src/screens/search/search_movie_delegate.dart';
import 'package:movie/src/shared/widgets/custom_circle_indicator.dart';
import 'package:movie/src/shared/widgets/disable_scroll_list.dart';
import 'package:movie/src/shared/widgets/movie_detail.dart';
import 'package:movie/src/shared/widgets/movie_pagination_completed_view.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});
  static const routeName = 'home-scren';

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late final ScrollController scrollController;

  @override
  void initState() {
    scrollController = ScrollController();
    setupScrollController();
    super.initState();
  }

  void setupScrollController() {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          context.read<MovieBloc>().add(const GetMovies());
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(80),
        child: AppBar(
          centerTitle: true,
          leadingWidth: 80,
          leading: Container(
            margin: const EdgeInsets.only(left: 10),
            child: IconButton(
              onPressed: () {},
              icon: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: Image.asset(
                  imageProfile,
                  width: 40,
                  height: 90,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          title: Text(
            S.of(context).home_appbar_title,
            style: Theme.of(context).textTheme.titleLarge,
          ),
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 10),
              child: IconButton(
                onPressed: invoke,
                icon: const Icon(Icons.search),
              ),
            )
          ],
        ),
      ),
      body: BlocBuilder<MovieBloc, MovieState>(
        builder: (context, state) {
          if (state is MovieLoading && state.isFirstFetch) {
            return const Center(
              child: CustomCircleIndicator(),
            );
          }

          var movies = <MovieSimplified>[];
          var isLoading = false;
          var paginationCompleted = false;
          var withoutResult = false;

          if (state is MovieLoading) {
            movies = state.oldMovies;
            isLoading = true;
          } else if (state is MovieLoaded) {
            movies = state.movies;
            paginationCompleted = state.complete;
            withoutResult = state.withoutResult;
          }

          if (withoutResult) {
            return const WithoutResultView();
          }

          return DisableScrollList(
            child: ListView.builder(
              controller: scrollController,
              physics: const ClampingScrollPhysics(),
              itemCount:
                  movies.length + (isLoading || paginationCompleted ? 1 : 0),
              itemBuilder: (context, index) {
                if (index < movies.length) {
                  return MovieItem(
                    movies[index],
                    onTap: () => Navigator.pushNamed(
                      context,
                      DetailScreen.routeName,
                      arguments: movies[index].id,
                    ),
                  );
                } else if (paginationCompleted) {
                  return const MoviePaginationCompletedView();
                } else {
                  return const Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: CustomCircleIndicator(),
                  );
                }
              },
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  void invoke() {
    showSearch(
      context: context,
      delegate: SearchMovieDelegate(
        searchFieldLabel: S.of(context).imput_search_movie,
      ),
    );
  }
}
