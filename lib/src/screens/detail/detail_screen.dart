import 'dart:developer';

import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie/src/screens/detail/bloc/detail_bloc/detail_bloc.dart';
import 'package:movie/src/screens/detail/widgets/detail_movie_image.dart';
import 'package:movie/src/shared/widgets/custom_circle_indicator.dart';
import 'package:movie/src/shared/widgets/custom_snackbar.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({super.key});
  static const routeName = 'detail-scren';

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: BlocConsumer<DetailBloc, DetailState>(
        listener: (context, state) {
          if (state is MovieError) {
            log(state.message);
            CustomSnackBar.show(context, message: state.message);
          }
        },
        builder: (context, state) {
          if (state is MovieLoading) {
            return const Center(
              child: CustomCircleIndicator(),
            );
          } else if (state is MovieLoaded) {
            final movie = state.movie as Movie;
            return SizedBox(
              width: width,
              height: height,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MovieImage(
                      height: height,
                      posterPath: movie.posterPath,
                      title: movie.title,
                    ),
                    MovieContent(movie: movie)
                  ],
                ),
              ),
            );
          } else {
            final error = state as MovieError;
            return Center(child: Text(error.message));
          }
        },
      ),
    );
  }
}
