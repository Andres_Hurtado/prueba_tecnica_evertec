import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:infrastructure/infrastructure.dart';
import 'package:movie/src/shared/theme.dart';
import 'package:movie/src/shared/widgets/custom_cached_image.dart';

class MovieImage extends StatelessWidget {
  const MovieImage({
    super.key,
    required this.height,
    required this.posterPath,
    required this.title,
  });

  final double height;
  final String posterPath;
  final String title;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height * .5,
      child: Stack(
        children: [
          if (posterPath.isNotEmpty)
            CustomCachedImage(
              url: Env.baseImageUrl + posterPath,
              borderRadius: BorderRadius.zero,
            ),
          if (posterPath.isEmpty)
            const ColoredBox(
              color: MovieTheme.greyColor,
              child: Icon(Icons.image),
            ),
          _Title(title: title),
          const _BackButton(),
        ],
      ),
    );
  }
}

class _Title extends StatelessWidget {
  const _Title({
    required this.title,
  });

  final String title;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 20,
      bottom: 20,
      child: Text(
        title,
        style: Theme.of(context).textTheme.titleLarge?.copyWith(
              fontSize: 20,
              color: MovieTheme.whiteColor,
              fontWeight: FontWeight.bold,
            ),
      ),
    );
  }
}

class _BackButton extends StatelessWidget {
  const _BackButton();

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 20,
      top: 60,
      child: CircleAvatar(
        radius: 20,
        backgroundColor: MovieTheme.whiteColor.withOpacity(0.7),
        child: Container(
          margin: const EdgeInsets.only(left: 5),
          child: BackButton(color: MovieTheme.blackColor.withOpacity(0.7)),
        ),
      ),
    );
  }
}

class MovieContent extends StatelessWidget {
  const MovieContent({
    super.key,
    required this.movie,
  });

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 20,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Descripcion',
            style: Theme.of(context).textTheme.bodySmall?.copyWith(
                  fontSize: 23,
                  fontWeight: FontWeight.bold,
                ),
          ),
          Text(movie.overview),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (movie.genres.isNotEmpty)
                Text(
                  'Generos',
                  style: Theme.of(context).textTheme.bodySmall?.copyWith(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                ),
              if (movie.genres.isNotEmpty)
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 100,
                  child: ListView.builder(
                    itemCount: movie.genres.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) => Padding(
                      padding: const EdgeInsets.all(8),
                      child: Container(
                        width: 100,
                        height: 40,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.primary,
                        ),
                        child: Text(
                          movie.genres[index],
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium
                              ?.copyWith(color: MovieTheme.whiteColor),
                        ),
                      ),
                    ),
                  ),
                )
            ],
          )
        ],
      ),
    );
  }
}
