import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:domain/domain.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:infrastructure/infrastructure.dart';
import 'package:injectable/injectable.dart';
import 'package:movie/src/dependencyInjection/dependency_injection.dart';

part 'detail_event.dart';
part 'detail_state.dart';

@injectable
class DetailBloc extends Bloc<DetailEvent, DetailState> {
  DetailBloc({required MovieService movieService})
      : _movieService = movieService,
        super(MovieLoading()) {
    on<GetMovie>(_onGetMovieEvent);
  }

  final MovieService _movieService;

  Future<void> _onGetMovieEvent(
    GetMovie event,
    Emitter<DetailState> emit,
  ) async {
    emit(MovieLoading());
    try {
      final movie = await _movieService.getMovie(event.id);
      emit(MovieLoaded(movie));
    } on NotConnectedToNetworkException catch (e) {
      emit(MovieError(e.message));
    } on HttpClientException catch (e) {
      emit(MovieError(e.message));
    } on RequiredKeyException catch (e) {
      emit(MovieError(e.message));
    } catch (e) {
      final message = e.toString();
      emit(MovieError(message));
    }
  }

  @override
  Future<void> close() {
    debugPrint('DetailBloc - close $_movieService');
    if (getIt.isRegistered<DetailBloc>(instance: this)) {
      getIt.reset();
    }
    return super.close();
  }
}
