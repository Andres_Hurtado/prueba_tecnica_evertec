part of 'detail_bloc.dart';

sealed class DetailEvent extends Equatable {
  const DetailEvent();
  @override
  List<Object> get props => [];
}

class GetMovie extends DetailEvent {
  const GetMovie(this.id);
  final int id;
  @override
  List<Object> get props => [id];
}
