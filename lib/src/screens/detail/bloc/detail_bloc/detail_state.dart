part of 'detail_bloc.dart';

sealed class DetailState extends Equatable {
  const DetailState();

  @override
  List<Object> get props => [];
}

class MovieLoading extends DetailState {}

class MovieLoaded extends DetailState {
  const MovieLoaded(this.movie);
  final MovieSimplified movie;

  @override
  List<Object> get props => [movie];
}

class MovieError extends DetailState {
  const MovieError(this.message);
  final String message;
  @override
  List<Object> get props => [message];
}
