import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie/src/screens/detail/detail_screen.dart';
import 'package:movie/src/screens/search/bloc/search/search_movie__bloc.dart';
import 'package:movie/src/shared/widgets/custom_circle_indicator.dart';
import 'package:movie/src/shared/widgets/disable_scroll_list.dart';
import 'package:movie/src/shared/widgets/movie_detail.dart';
import 'package:movie/src/shared/widgets/movie_pagination_completed_view.dart';

class SearchView extends StatefulWidget {
  const SearchView({super.key, required this.textQuery});
  final String textQuery;

  @override
  State<SearchView> createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  late final ScrollController scrollController;

  @override
  void initState() {
    scrollController = ScrollController();
    setupScrollController();
    super.initState();
  }

  void setupScrollController() {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          context.read<SearchMovieBloc>().add(const SearchMovie(textQuery: ''));
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchMovieBloc, SearchMovieState>(
      builder: (context, state) {
        if (state is SearchMovieLoading && state.isFirstFetch) {
          return const Center(
            child: CustomCircleIndicator(),
          );
        }

        var movies = <MovieSimplified>[];
        var isLoading = false;
        var paginationCompleted = false;
        var withoutResult = false;

        if (state is SearchMovieLoading) {
          movies = state.oldMovies;
          isLoading = true;
        } else if (state is SearchMovieLoaded) {
          movies = state.movies;
          paginationCompleted = state.complete;
          withoutResult = state.withoutResult;
        }

        if (withoutResult) {
          return const WithoutResultView();
        }

        return DisableScrollList(
          child: ListView.builder(
            controller: scrollController,
            physics: const ClampingScrollPhysics(),
            itemCount:
                movies.length + (isLoading || paginationCompleted ? 1 : 0),
            itemBuilder: (context, index) {
              if (index < movies.length) {
                return MovieItem(
                  movies[index],
                  onTap: () => Navigator.pushNamed(
                    context,
                    DetailScreen.routeName,
                    arguments: movies[index].id,
                  ),
                );
              } else if (paginationCompleted) {
                return const MoviePaginationCompletedView();
              } else {
                return const Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: CustomCircleIndicator(),
                );
              }
            },
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }
}
