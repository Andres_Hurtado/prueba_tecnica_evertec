import 'package:flutter/material.dart';

class InputEMptyView extends StatelessWidget {
  const InputEMptyView({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Agrega una palabra en el campo de busqueda...'),
    );
  }
}
