import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie/src/dependencyInjection/dependency_injection.dart';
import 'package:movie/src/screens/search/bloc/search/search_movie__bloc.dart';
import 'package:movie/src/screens/search/search_view.dart';
import 'package:movie/src/screens/search/widgets/input_empty_view.dart';
import 'package:movie/src/shared/theme.dart';

class SearchMovieDelegate extends SearchDelegate<void> {
  SearchMovieDelegate({
    required this.searchFieldLabel,
  }) {
    _bloc = getIt<SearchMovieBloc>();
  }

  @override
  // ignore: overridden_fields
  final String searchFieldLabel;

  late final SearchMovieBloc _bloc;

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => query = '',
        icon: Icon(
          Icons.backspace,
          size: 35,
          color: Theme.of(context).colorScheme.primary,
        ),
      )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return BackButton(
      color: MovieTheme.blackColor,
      style: const ButtonStyle(
        iconSize: MaterialStatePropertyAll(35),
      ),
      onPressed: () => close(context, null),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query.isEmpty) {
      return const InputEMptyView();
    }

    SchedulerBinding.instance.addPostFrameCallback((_) {
      _bloc.add(SearchMovie(textQuery: query, resetThereIsNewPagination: true));
    });

    return BlocProvider.value(
      value: getIt<SearchMovieBloc>(),
      child: SearchView(textQuery: query),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    //* Codigo, para sugerir peliculas....
    return Container();
  }
}
