part of 'search_movie__bloc.dart';

sealed class SearchMovieEvent extends Equatable {
  const SearchMovieEvent();

  @override
  List<Object> get props => [];
}

class SearchMovie extends SearchMovieEvent {
  const SearchMovie({
    required this.textQuery,
    this.resetThereIsNewPagination = false,
  });
  final String textQuery;
  final bool resetThereIsNewPagination;
  @override
  List<Object> get props => [textQuery, resetThereIsNewPagination];
}
