part of 'search_movie__bloc.dart';

sealed class SearchMovieState extends Equatable {
  const SearchMovieState();

  @override
  List<Object> get props => [];
}

final class SearchMovieInitial extends SearchMovieState {}

class SearchMovieLoading extends SearchMovieState {
  const SearchMovieLoading(this.oldMovies, {required this.isFirstFetch});

  final List<MovieSimplified> oldMovies;
  final bool isFirstFetch;

  @override
  List<Object> get props => [oldMovies, isFirstFetch];
}

class SearchMovieLoaded extends SearchMovieState {
  const SearchMovieLoaded(
    this.movies, {
    this.complete = false,
    this.withoutResult = false,
  });

  final List<MovieSimplified> movies;
  final bool complete;
  final bool withoutResult;

  @override
  List<Object> get props => [movies, complete, withoutResult];
}

class SearchMovieError extends SearchMovieState {
  const SearchMovieError(this.message);
  final String message;
  @override
  List<Object> get props => [message];
}
