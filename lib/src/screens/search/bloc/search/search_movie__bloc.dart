import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:domain/domain.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:infrastructure/infrastructure.dart';
import 'package:injectable/injectable.dart';
import 'package:movie/src/dependencyInjection/dependency_injection.dart';

part 'search_movie_event.dart';
part 'search_movie_state.dart';

@LazySingleton()
class SearchMovieBloc extends Bloc<SearchMovieEvent, SearchMovieState> {
  SearchMovieBloc({required MovieService movieService})
      : _movieService = movieService,
        super(SearchMovieInitial()) {
    on<SearchMovie>(_onSearchMovieEvent);
  }

  final MovieService _movieService;

  int page = 1;

  Future<void> _onSearchMovieEvent(
    SearchMovie event,
    Emitter<SearchMovieState> emit,
  ) async {
    //* Nos permite resetear el state,
    //* ya que hay un cambio de tipo animal.
    if (event.resetThereIsNewPagination) {
      page = 1;
      emit(SearchMovieInitial());
    }

    if (state is SearchMovieLoading) return;
    if (state is SearchMovieLoaded && (state as SearchMovieLoaded).complete) {
      return;
    }

    final currentState = state;
    var oldMovies = <MovieSimplified>[];

    if (currentState is SearchMovieLoaded) {
      oldMovies = currentState.movies;
    }

    emit(SearchMovieLoading(oldMovies, isFirstFetch: page == 1));

    try {
      final newMovies =
          await _movieService.searchMovie(page, textQuery: event.textQuery);
    log('LLAMADO');

      //* Validamos que la consulta nos traiga datos,
      //* en caso de que sea vacio,
      //* damos por hecho de que ya no hay mas datos,
      //* por lo tanto notificamos con la vaible complete.
      if (newMovies.isEmpty) {
        final oldMovies = (state as SearchMovieLoading).oldMovies;
        final isFirstFetch = (state as SearchMovieLoading).isFirstFetch;

        if (!isFirstFetch) {
          emit(SearchMovieLoaded(oldMovies, complete: true));
          return;
        } else {
          emit(SearchMovieLoaded(oldMovies, withoutResult: true));
          return;
        }
      } else {
        //* Aumentamos la variable page, para futura, paginacion.
        page++;
      }

      //* Obtenemos los datos guardados de las peliculas,
      //* junto a su vez tenemos las nuevas peliculas ya que se los insertamos.
      final oldMovies = (state as SearchMovieLoading).oldMovies
        ..addAll(newMovies);

      //* Cambiamos de estado con todos los datos.
      emit(SearchMovieLoaded(oldMovies));
    } on NotConnectedToNetworkException catch (e) {
      emit(SearchMovieError(e.message));
    } on RequiredKeyException catch (e) {
      emit(SearchMovieError(e.message));
    } on HttpClientException catch (e) {
      emit(SearchMovieError(e.message));
    } catch (e) {
      final message = e.toString();
      emit(SearchMovieError(message));
    }
  }

  @override
  Future<void> close() {
    debugPrint('MovieBloc - close $_movieService');
    if (getIt.isRegistered<SearchMovieBloc>(instance: this)) {
      getIt.resetLazySingleton<SearchMovieBloc>(instance: this);
    }
    return super.close();
  }
}
