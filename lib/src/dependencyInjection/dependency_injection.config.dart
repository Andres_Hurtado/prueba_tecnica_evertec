// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:domain/domain.dart' as _i4;
import 'package:domain/domain.module.dart' as _i8;
import 'package:get_it/get_it.dart' as _i1;
import 'package:infrastructure/infrastructure.module.dart' as _i7;
import 'package:injectable/injectable.dart' as _i2;
import 'package:movie/src/screens/detail/bloc/detail_bloc/detail_bloc.dart'
    as _i3;
import 'package:movie/src/screens/home/bloc/home/movie_bloc.dart' as _i5;
import 'package:movie/src/screens/search/bloc/search/search_movie__bloc.dart'
    as _i6;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  Future<_i1.GetIt> init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) async {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.factory<_i3.DetailBloc>(
        () => _i3.DetailBloc(movieService: gh<_i4.MovieService>()));
    gh.lazySingleton<_i5.MovieBloc>(
        () => _i5.MovieBloc(movieService: gh<_i4.MovieService>()));
    gh.lazySingleton<_i6.SearchMovieBloc>(
        () => _i6.SearchMovieBloc(movieService: gh<_i4.MovieService>()));
    await _i7.InfrastructurePackageModule().init(gh);
    await _i8.DomainPackageModule().init(gh);
    return this;
  }
}
