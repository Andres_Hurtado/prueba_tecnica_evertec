import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:infrastructure/infrastructure.dart';
import 'package:movie/src/shared/date_format/date_format.dart';
import 'package:movie/src/shared/theme.dart';

class MovieItem extends StatelessWidget {
  const MovieItem(this.movie, {super.key, required this.onTap});
  final MovieSimplified movie;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 20,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: Stack(
            children: [
              if (movie.posterPath.isNotEmpty)
                Image.network(Env.baseImageUrl + movie.posterPath),
              if (movie.posterPath.isEmpty)
                const ColoredBox(
                  color: MovieTheme.greyColor,
                  child: Icon(Icons.image),
                ),
              Positioned(
                right: 20,
                top: 30,
                child: SizedBox(
                  height: 50,
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          //* Logica favorite...
                        },
                        icon: const Icon(
                          Icons.favorite,
                          color: MovieTheme.whiteColor,
                          size: 40,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Positioned(
                left: 10,
                right: 10,
                bottom: 5,
                child: Container(
                  height: 70,
                  color: MovieTheme.blackColor.withOpacity(1),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        height: 40,
                        child: _TitleImage(title: movie.title),
                      ),
                      SizedBox(
                        height: 60,
                        child: _TitleBannerImage(
                          title: ConvertDateFormat.formt(movie.releaseDate),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _TitleImage extends StatelessWidget {
  const _TitleImage({required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.4,
      height: 40,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: MovieTheme.whiteColor),
      ),
      child: Text(
        title,
        overflow: TextOverflow.ellipsis,
        style: Theme.of(context)
            .textTheme
            .titleMedium
            ?.copyWith(color: MovieTheme.whiteColor),
      ),
    );
  }
}

class _TitleBannerImage extends StatelessWidget {
  const _TitleBannerImage({required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.4,
      height: 40,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: const Color.fromARGB(255, 228, 171, 2),
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: MovieTheme.whiteColor),
      ),
      child: Text(
        title,
        overflow: TextOverflow.ellipsis,
        style: Theme.of(context)
            .textTheme
            .titleMedium
            ?.copyWith(color: MovieTheme.whiteColor),
      ),
    );
  }
}

class WithoutResultView extends StatelessWidget {
  const WithoutResultView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
