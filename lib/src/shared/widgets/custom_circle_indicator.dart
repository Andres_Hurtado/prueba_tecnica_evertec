import 'package:flutter/material.dart';
import 'package:movie/src/shared/theme.dart';

class CustomCircleIndicator extends StatelessWidget {
  const CustomCircleIndicator({
    super.key,
    this.color = MovieTheme.tertiaryColor,
  });
  final Color color;

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Colors.transparent,
      child: CircularProgressIndicator(
        color: color,
      ),
    );
  }
}
