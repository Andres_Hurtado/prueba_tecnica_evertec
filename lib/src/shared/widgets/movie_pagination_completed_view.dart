import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:movie/generated/l10n.dart';

class MoviePaginationCompletedView extends StatelessWidget {
  const MoviePaginationCompletedView({super.key});

  @override
  Widget build(BuildContext context) {
    return FadeInUp(
      child: Container(
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.center,
        margin: const EdgeInsets.only(top: 30),
        child: Text(
          S.of(context).pagination_completed,
          style: Theme.of(context).textTheme.titleSmall,
        ),
      ),
    );
  }
}
