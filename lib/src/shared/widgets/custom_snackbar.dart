
import 'package:flutter/material.dart';

class CustomSnackBar {
  static void show(BuildContext context, {required String message}) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message)));
  }
}
