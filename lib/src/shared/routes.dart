import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie/src/dependencyInjection/dependency_injection.dart';
import 'package:movie/src/screens/detail/bloc/detail_bloc/detail_bloc.dart';
import 'package:movie/src/screens/detail/detail_screen.dart';
import 'package:movie/src/screens/home/bloc/home/movie_bloc.dart';
import 'package:movie/src/screens/home/home_screen.dart';
import 'package:movie/src/screens/search/bloc/search/search_movie__bloc.dart';
import 'package:movie/src/screens/splash/splash_screen.dart';

RouteFactory onGenerateRoute = (RouteSettings settings) {
  final name = settings.name;

  if (name == SplashScreen.routeName) {
    const transitionDuration = Duration(milliseconds: 150);

    return PageRouteBuilder(
      transitionDuration: transitionDuration,
      reverseTransitionDuration: transitionDuration,
      pageBuilder: (_, animation, ___) {
        return FadeTransition(
          opacity: animation,
          child: const SplashScreen(),
        );
      },
    );
  }

  if (name == HomeScreen.routeName) {
    return MaterialPageRoute(
      builder: (context) => MultiBlocProvider(
        providers: [
          BlocProvider(
            //* Proveemos el Bloc, y asu vez solicitamos las peliculas.
            create: (context) => getIt<MovieBloc>()..add(const GetMovies()),
          ),
          BlocProvider(
            create: (context) => getIt<SearchMovieBloc>(),
          ),
        ],
        child: const HomeScreen(),
      ),
    );
  }

  if (name == DetailScreen.routeName) {
    const transitionDuration = Duration(milliseconds: 150);
    //* Argumento id requerido para poder obtener la pelicula
    final arguments = settings.arguments;
    if (arguments == null) return null;
    final id = arguments as int;
    return PageRouteBuilder(
      transitionDuration: transitionDuration,
      reverseTransitionDuration: transitionDuration,
      pageBuilder: (_, animation, ___) {
        return FadeTransition(
          opacity: animation,
          child: BlocProvider(
            create: (_) => getIt<DetailBloc>()..add(GetMovie(id)),
            child: const DetailScreen(),
          ),
        );
      },
    );
  }

  return null;
};
