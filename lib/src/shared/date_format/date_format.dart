import 'package:intl/intl.dart';

class ConvertDateFormat {
  static String formt(DateTime date) {
    return DateFormat('dd-MMMM-yyyy').format(date);
  }
}
