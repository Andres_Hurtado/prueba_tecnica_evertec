mixin Builder<T> {
  T build();
}
