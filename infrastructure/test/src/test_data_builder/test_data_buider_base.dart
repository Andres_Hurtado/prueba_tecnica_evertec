import 'test_data_buider.dart';

abstract class BaseBuilder<T> implements Builder<T> {
  abstract T data;

  @override
  T build() => this.data;
}
