import 'package:drift/native.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:infrastructure/src/movie/anticorruption/movie_simplified_translator.dart';
import 'package:infrastructure/src/movie/anticorruption/movie_translator.dart';
import 'package:infrastructure/src/movie/persistence/dao/movie_dao.dart';
import 'package:infrastructure/src/shared/persistence/movie_database.dart';

import '../../test_data_builder/test_data_base_builder_movie.dart';
import '../../test_data_builder/test_data_base_builder_movie_simplified.dart';
import '../../test_data_builder/test_data_builder_movie.dart';
import '../../test_data_builder/test_data_builder_movie_simplified.dart';

void main() {
  late MovieDatabase db;
  late MovieDao movieDao;

  setUp(() {
    db = MovieDatabase.withQueryEx(NativeDatabase.memory());
    movieDao = MovieDao(db);
  });
  tearDown(() async {
    await db.close();
  });

  group('GetMovieAll', () {
    test('GetMovieAll_MustHaveAllMovies_success', () async {
      //Arrange
      const page = 1;
      final listMovieEntity = List.generate(10, (index) {
        final builderSimplified = MovieSimplifiedBuilder()
          ..id = index
          ..originalTitle = 'prueba'
          ..posterPath = 'imagen'
          ..releaseDate = DateTime.now()
          ..title = 'prueba';

        final movieSimplified =
            BaseBuiderMovieSimplified(builderSimplified).build();
        return MovieSimplifiedTranslator.fromDomainToDao(movieSimplified);
      });
      //Act
      await movieDao.insertMovies(listMovieEntity);
      final result = await movieDao.getMovieAll(page);
      //Assert
      expect(result, isA<List<MovieEntityData>>());
      expect(result, isNotEmpty);
    });

    test('GetMovie_MustHaveMovie_success', () async {
      //Arrange
      final builder = MovieBuilder()
        ..id = 130
        ..originalTitle = 'prueba'
        ..posterPath = 'imagen'
        ..releaseDate = DateTime.now()
        ..overview = ''
        ..popularity = 0
        ..genres = ['d', 's']
        ..title = 'prueba'
        ..createdAt = DateTime.now()
        ..updatedAt = DateTime.now();

      final movie = BaseBuiderMovie(builder).build();
      final movieDataDao = MovieTranslator.fromDomainToDao(movie);
      //Act

      await movieDao.insertMovies([movieDataDao]);
      final result = await movieDao.getMovieById(movie.id);
      //Assert
      expect(result, isA<MovieEntityData>());
    });
  });
}
