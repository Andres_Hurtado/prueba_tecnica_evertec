import 'package:domain/src/movie/model/movie_simplified.dart';
import '../../test_data_builder/test_data_buider_base.dart';
import 'test_data_builder_movie_simplified.dart';

//* Nos permite tener crear la instancia de la clase Movie Simplfied,
//* obteniendo los datos atraves de su funcion builder.
class BaseBuiderMovieSimplified extends BaseBuilder<MovieSimplified> {
  BaseBuiderMovieSimplified(MovieSimplifiedBuilder builder)
      : id = builder.id,
        originalTitle = builder.originalTitle,
        posterPath = builder.posterPath,
        releaseDate = builder.releaseDate,
        title = builder.title;

  final int id;
  final String originalTitle;
  final String posterPath;
  final DateTime releaseDate;
  final String title;

  @override
  late MovieSimplified data;

  @override
  MovieSimplified build() {
    data = MovieSimplified(
      id: id,
      title: title,
      posterPath: posterPath,
      releaseDate: releaseDate,
      originalTitle: originalTitle,
    );
    return super.build();
  }
}
