import 'package:domain/src/movie/model/movie.dart';
import '../../test_data_builder/test_data_buider_base.dart';
import 'test_data_builder_movie.dart';

class BaseBuiderMovie extends BaseBuilder<Movie> {
  BaseBuiderMovie(MovieBuilder builder)
      : id = builder.id,
        originalTitle = builder.originalTitle,
        overview = builder.overview,
        popularity = builder.popularity,
        posterPath = builder.posterPath,
        releaseDate = builder.releaseDate,
        title = builder.title,
        genres = builder.genres,
        createdAt = builder.createdAt,
        updatedAt = builder.updatedAt;

  final int id;
  final String originalTitle;
  final String overview;
  final double popularity;
  final String posterPath;
  final DateTime releaseDate;
  final String title;
  final List<String> genres;
  final DateTime createdAt;
  final DateTime updatedAt;

  @override
  late Movie data;

  @override
  Movie build() {
    data = Movie(
      id: id,
      title: title,
      posterPath: posterPath,
      popularity: popularity,
      releaseDate: releaseDate,
      overview: overview,
      originalTitle: originalTitle,
      genres: genres,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
    return super.build();
  }
}
