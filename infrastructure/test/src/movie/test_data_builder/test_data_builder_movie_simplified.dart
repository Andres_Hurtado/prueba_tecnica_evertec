class MovieSimplifiedBuilder {
  late int id;
  late String originalTitle;
  late String posterPath;
  late DateTime releaseDate;
  late String title;
}
