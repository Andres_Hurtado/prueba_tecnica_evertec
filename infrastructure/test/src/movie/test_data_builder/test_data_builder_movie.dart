class MovieBuilder {
  late int id;
  late String originalTitle;
  late String overview;
  late double popularity;
  late String posterPath;
  late DateTime releaseDate;
  late String title;
  late List<String> genres;
  late DateTime createdAt;
  late DateTime updatedAt;
}
