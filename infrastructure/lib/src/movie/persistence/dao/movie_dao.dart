import 'dart:developer';

import 'package:drift/drift.dart';
import 'package:infrastructure/src/movie/persistence/entity/movie_entity.dart';
import 'package:infrastructure/src/shared/persistence/movie_database.dart';
import 'package:injectable/injectable.dart';

part 'movie_dao.g.dart';

@DriftAccessor(tables: [MovieEntity])
@injectable
class MovieDao extends DatabaseAccessor<MovieDatabase> with _$MovieDaoMixin {
  MovieDao(super.db);

  Future<List<MovieEntityData>> getMovieAll(int page) async {
    const limit = 10;
    final offset = (page - 1) * limit;
    log(offset.toString());
    return (select(movieEntity)..limit(limit, offset: offset)).get();
  }

  Future<MovieEntityData?> getMovieById(int id) async {
    return transaction(() async {
      return (select(movieEntity)..where((t) => t.id.equals(id))).getSingleOrNull();
    });
  }

  Future<void> insertMovies(List<MovieEntityCompanion> movies) async {
    return transaction(() async {
      await delete(movieEntity).go();
      await batch((entity) {
        entity.insertAll(movieEntity, movies, mode: InsertMode.insertOrReplace);
      });
    });
  }

  Future<void> updateMovie(
    MovieEntityCompanion movie,
  ) async {
    return transaction(() async {
      await (update(movieEntity)..where((reg) => reg.id.equals(movie.id.value)))
          .write(movie);
    });
  }

  Future<int> getCountMovies() async {
    final count = movieEntity.title.count();
    final query = selectOnly(movieEntity)..addColumns([count]);
    return (await query.map((row) => row.read(count)).getSingle())!;
  }
}
