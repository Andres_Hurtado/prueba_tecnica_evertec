import 'package:drift/drift.dart';
import 'package:infrastructure/src/movie/anticorruption/movie_genre_translator.dart';

class MovieEntity extends Table {
  IntColumn get id => integer()();
  TextColumn get title => text()();
  TextColumn get originalTitle => text()();
  TextColumn get overview => text()();
  TextColumn get posterPath => text()();
  RealColumn get popularity => real().nullable()();
  DateTimeColumn get releaseDate => dateTime()();
  TextColumn get genres =>  text().map( const MovieGenreConverter()).nullable()();
  DateTimeColumn get createdAt => dateTime().withDefault(currentDateAndTime)();
  DateTimeColumn get updatedAt => dateTime().withDefault(currentDateAndTime)();

  @override
  Set<Column> get primaryKey => {id};
}
