import 'package:domain/domain.dart';

abstract interface class MovieLocalRepository {
  Future<List<MovieSimplified>> getMovies(int page);
  Future<Movie?> getMovieById(int id);
  Future<void> updateById(Movie movie);
  Future<void> insertMovies(List<MovieSimplified> movies);
  Future<bool> isEmpty();
}
