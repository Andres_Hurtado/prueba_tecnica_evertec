import 'package:domain/domain.dart';

abstract interface class MovieRemoteRepository {
  Future<List<MovieSimplified>> getMovies(int page);
  Future<Movie> getById(int id);
  Future<List<MovieSimplified>> searchMovie(
    int page, {
    required String textQuery,
  });
}
