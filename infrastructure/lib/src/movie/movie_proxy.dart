import 'package:domain/domain.dart';
import 'package:infrastructure/src/movie/contracts/movie_local_repository.dart';
import 'package:infrastructure/src/movie/contracts/movie_remote_repository.dart';
import 'package:infrastructure/src/movie/contracts/movie_temporal_repository.dart';
import 'package:infrastructure/src/shared/network/exceptions/not_connected_to_network.dart';
import 'package:infrastructure/src/shared/network/network_verify.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: MovieRepository)
class MovieProxy implements MovieRepository {
  MovieProxy({
    required MovieRemoteRepository movieRemoteRepository,
    required MovieLocalRepository movieLocalRepository,
    required NetworkVerify networkVerify,
    required MovieTemporalRepository movieTemporalRepository,
  })  : _networkVerify = networkVerify,
        _movieRemoteRepository = movieRemoteRepository,
        _movieLocalRepository = movieLocalRepository,
        _movieTemporalRepository = movieTemporalRepository;

  final NetworkVerify _networkVerify;
  final MovieRemoteRepository _movieRemoteRepository;
  final MovieLocalRepository _movieLocalRepository;
  final MovieTemporalRepository _movieTemporalRepository;

  //* Tiempo de permanencia de la data local,
  //* para ser actualizada, de la data remote.
  static const _millisPerDay = 3600000;

  @override
  Future<List<MovieSimplified>> get(int page) async {
    final sharedTime =
        await _movieTemporalRepository.getLastUpdatedPreference();
    final isEmptyLocal = await _movieLocalRepository.isEmpty();
    if (!isEmptyLocal && _isUpdated(sharedTime)) {
      var movies = const <MovieSimplified>[];
      movies = await _movieLocalRepository.getMovies(page);
      if (movies.isEmpty) {
        final response = await _movieRemoteRepository.getMovies(page);
        await _insertMovies(response);
        return response;
      }
      return movies;
    } else if (await _networkVerify.isConnected) {
      final response = await _movieRemoteRepository.getMovies(page);
      await _insertMovies(response);
      return response;
    } else {
      throw NotConnectedToNetworkException();
    }
  }

  @override
  Future<Movie> getById(int id) async {
    final movieLocal = await _movieLocalRepository.getMovieById(id);
    if (movieLocal != null &&
        _isSingleUpdated(
          movieLocalCreatedAt: movieLocal.createdAt,
          movieLocalUpdatedAt: movieLocal.updatedAt,
        )) {
      return movieLocal;
    } else {
      final movieRemote = await _movieRemoteRepository.getById(id);
      await _updateMovie(movieRemote);
      return movieRemote;
    }
  }

  Future<void> _insertMovies(List<MovieSimplified> movies) async {
    await _movieLocalRepository.insertMovies(movies);
    await _movieTemporalRepository.saveLastUpdatedPreference(
      DateTime.now().millisecondsSinceEpoch.toString(),
    );
  }

  @override
  Future<List<MovieSimplified>> searchMovie(
    int page, {
    required String textQuery,
  }) {
    return _movieRemoteRepository.searchMovie(page, textQuery: textQuery);
  }

  Future<void> _updateMovie(Movie movie) async =>
      _movieLocalRepository.updateById(movie);

  bool _isUpdated(String storeDate) {
    final actualDate = DateTime.now().millisecondsSinceEpoch;
    if (storeDate.isNotEmpty) {
      return (actualDate - int.parse(storeDate)) <= _millisPerDay;
    } else {
      return false;
    }
  }

  bool _isSingleUpdated({
    required DateTime movieLocalCreatedAt,
    required DateTime movieLocalUpdatedAt,
  }) {
    if (movieLocalUpdatedAt.compareTo(movieLocalCreatedAt) == 0) {
      return false;
    } else {
      return true;
    }
  }
}
