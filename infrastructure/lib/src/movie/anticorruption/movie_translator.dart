import 'package:domain/domain.dart' as domain;
import 'package:drift/drift.dart';
import 'package:infrastructure/src/movie/http/dto/movie_dto.dart';
import 'package:infrastructure/src/shared/persistence/movie_database.dart';

class MovieTranslator {
  static domain.Movie fromDtoToDomain(Movie movie) {
 
    return domain.Movie(
      id: movie.id,
      genres: movie.genres.map((genre) => genre.name ?? '').toList(),
      originalTitle: movie.originalTitle,
      overview: movie.overview,
      popularity: movie.popularity,
      posterPath: movie.posterPath ?? '',
      releaseDate: movie.releaseDate,
      title: movie.title,
      createdAt: DateTime.now(),
      updatedAt: DateTime.now(),
    );
  }

  static domain.Movie fromDaoToDomain(MovieEntityData movie) {
    return domain.Movie(
      id: movie.id,
      genres: movie.genres ?? [],
      originalTitle: movie.originalTitle,
      overview: movie.overview,
      popularity: movie.popularity ?? 0,
      posterPath: movie.posterPath,
      releaseDate: movie.releaseDate,
      title: movie.title,
      createdAt: movie.createdAt,
      updatedAt: movie.updatedAt,
    );
  }

  static MovieEntityCompanion fromDomainToDao(domain.Movie movie) {
    return MovieEntityCompanion(
      id: Value(movie.id),
      genres: Value(movie.genres),
      originalTitle: Value(movie.originalTitle),
      overview: Value(movie.overview),
      popularity: Value(movie.popularity),
      posterPath: Value(movie.posterPath),
      releaseDate: Value(movie.releaseDate),
      title: Value(movie.title),
      updatedAt: Value(DateTime.now()),
    );
  }
}
