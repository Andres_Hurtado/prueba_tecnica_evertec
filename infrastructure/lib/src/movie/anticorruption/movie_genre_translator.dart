import 'dart:convert';
import 'package:drift/drift.dart';

class MovieGenreConverter extends TypeConverter<List<String>?, String?> {
  const MovieGenreConverter();

  @override
  List<String>? fromSql(String? fromDb) {
    if (fromDb == null) return null;
    final data = json.decode(fromDb) as List;
    return data.map((item) => item.toString()).toList();
  }

  @override
  String? toSql(List<String>? value) {
    if (value == null) return null;
    return json.encode(value);
  }
}
