import 'package:domain/domain.dart' as domain;
import 'package:drift/drift.dart';
import 'package:infrastructure/src/movie/http/dto/movie_simplified_dto.dart';
import 'package:infrastructure/src/shared/persistence/movie_database.dart';

class MovieSimplifiedTranslator {
  static domain.MovieSimplified fromDtoToDomain(MovieSimplified movie) {
    return domain.MovieSimplified(
      id: movie.id,
      originalTitle: movie.originalTitle,
      posterPath: movie.posterPath ?? '',
      releaseDate: movie.releaseDate,
      title: movie.title,
    );
  }

  static domain.MovieSimplified fromDaoToDomain(MovieEntityData movie) {
    return domain.MovieSimplified(
      id: movie.id,
      originalTitle: movie.originalTitle,
      posterPath: movie.posterPath,
      releaseDate: movie.releaseDate,
      title: movie.title,
    );
  }

  static MovieEntityCompanion fromDomainToDao(domain.MovieSimplified movie) {
    return MovieEntityCompanion(
      id: Value(movie.id),
      originalTitle: Value(movie.originalTitle),
      posterPath: Value(movie.posterPath),
      releaseDate: Value(movie.releaseDate),
      title: Value(movie.title),
      overview: const Value(''),
      popularity: const Value(0),
    );
  }
}
