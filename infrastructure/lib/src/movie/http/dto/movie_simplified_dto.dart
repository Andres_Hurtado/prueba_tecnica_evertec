import 'package:json_annotation/json_annotation.dart';

part 'movie_simplified_dto.g.dart';

@JsonSerializable()
class MovieSimplified {
  MovieSimplified({
    required this.adult,
    required this.id,
    required this.originalLanguage,
    required this.originalTitle,
    required this.overview,
    required this.popularity,
    required this.posterPath,
    required this.releaseDate,
    required this.title,
    required this.voteAverage,
    required this.voteCount,
    required this.backdropPath,
    required this.genreIds,
    required this.video,
  });

  factory MovieSimplified.fromJson(Map<String, dynamic> json) =>
      _$MovieSimplifiedFromJson(json);

  final bool adult;
  @JsonKey(name: 'backdrop_path')
  final String? backdropPath;
  @JsonKey(name: 'genre_ids')
  final List<int>? genreIds;
  final int id;
  @JsonKey(name: 'original_language')
  final String originalLanguage;
  @JsonKey(name: 'original_title')
  final String originalTitle;
  final String overview;
  final double popularity;
  @JsonKey(name: 'poster_path')
  final String? posterPath;
  @JsonKey(name: 'release_date')
  final DateTime releaseDate;
  final String title;
  final bool video;
  @JsonKey(name: 'vote_average')
  final double voteAverage;
  @JsonKey(name: 'vote_count')
  final int voteCount;

  Map<String, dynamic> toJson() => _$MovieSimplifiedToJson(this);
}
