// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_pagination_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MoviePagination _$MoviePaginationFromJson(Map<String, dynamic> json) =>
    MoviePagination(
      page: json['page'] as int,
      results: (json['results'] as List<dynamic>)
          .map((e) => MovieSimplified.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalPages: json['total_pages'] as int,
      totalResults: json['total_results'] as int,
    );

Map<String, dynamic> _$MoviePaginationToJson(MoviePagination instance) =>
    <String, dynamic>{
      'page': instance.page,
      'results': MoviePagination._toJsonMovies(instance.results),
      'total_pages': instance.totalPages,
      'total_results': instance.totalResults,
    };
