import 'package:json_annotation/json_annotation.dart';
part 'movie_country_dto.g.dart';

@JsonSerializable()
class ProductionCountry {
  ProductionCountry({
    required this.iso31661,
    required this.name,
  });

  factory ProductionCountry.fromJson(Map<String, dynamic> json) =>
      _$ProductionCountryFromJson(json);

  @JsonKey(name: 'iso_3166_1')
  final String iso31661;
  final String name;

  Map<String, dynamic> toJson() => _$ProductionCountryToJson(this);
}
