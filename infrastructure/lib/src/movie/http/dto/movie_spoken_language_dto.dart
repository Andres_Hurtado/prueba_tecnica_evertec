import 'package:json_annotation/json_annotation.dart';

part 'movie_spoken_language_dto.g.dart';

@JsonSerializable()
class SpokenLanguage {
  SpokenLanguage({
    required this.englishName,
    required this.iso6391,
    required this.name,
  });

  factory SpokenLanguage.fromJson(Map<String, dynamic> json) =>
      _$SpokenLanguageFromJson(json);

  @JsonKey(name: 'english_name')
  final String englishName;
  @JsonKey(name: 'iso_639_1')
  final String iso6391;
  final String name;

  Map<String, dynamic> toJson() => _$SpokenLanguageToJson(this);
}
