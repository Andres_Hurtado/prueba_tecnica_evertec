import 'package:json_annotation/json_annotation.dart';
part 'movie_belongs_to_collection_dto.g.dart';

@JsonSerializable()
class BelongsToCollection {
  BelongsToCollection({
    required this.id,
    required this.name,
    required this.posterPath,
    required this.backdropPath,
  });
  factory BelongsToCollection.fromJson(Map<String, dynamic> json) =>
      _$BelongsToCollectionFromJson(json);

  final int id;
  final String? name;
  @JsonKey(name: 'poster_path')
  final String? posterPath;
  @JsonKey(name: 'backdrop_path')
  final String? backdropPath;

  Map<String, dynamic> toJson() => _$BelongsToCollectionToJson(this);
}
