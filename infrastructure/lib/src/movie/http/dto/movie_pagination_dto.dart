import 'package:infrastructure/src/movie/http/dto/movie_simplified_dto.dart';
import 'package:json_annotation/json_annotation.dart';

part 'movie_pagination_dto.g.dart';

@JsonSerializable()
class MoviePagination {
  MoviePagination({
    required this.page,
    required this.results,
    required this.totalPages,
    required this.totalResults,
  });

  factory MoviePagination.fromJson(Map<String, dynamic> json) =>
      _$MoviePaginationFromJson(json);

  final int page;
  @JsonKey(toJson: _toJsonMovies)
  final List<MovieSimplified> results;
  @JsonKey(name: 'total_pages')
  final int totalPages;
  @JsonKey(name: 'total_results')
  final int totalResults;

  Map<String, dynamic> toJson() => _$MoviePaginationToJson(this);

  static List<Map<String, dynamic>> _toJsonMovies(
    List<MovieSimplified> movies,
  ) =>
      movies.map((movie) => movie.toJson()).toList();
}
