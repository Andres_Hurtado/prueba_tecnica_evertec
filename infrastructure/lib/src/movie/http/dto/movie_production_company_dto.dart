import 'package:json_annotation/json_annotation.dart';
part 'movie_production_company_dto.g.dart';

@JsonSerializable()
class ProductionCompany {
  ProductionCompany({
    required this.id,
    required this.logoPath,
    required this.name,
    required this.originCountry,
  });

  factory ProductionCompany.fromJson(Map<String, dynamic> json) =>
      _$ProductionCompanyFromJson(json);

  final int id;
  @JsonKey(name: 'logo_path')
  final String? logoPath;
  final String? name;
  @JsonKey(name: 'origin_country')
  final String? originCountry;

  Map<String, dynamic> toJson() => _$ProductionCompanyToJson(this);
}
