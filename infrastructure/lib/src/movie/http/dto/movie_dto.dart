import 'package:infrastructure/src/movie/http/dto/movie_belongs_to_collection_dto.dart';
import 'package:infrastructure/src/movie/http/dto/movie_country_dto.dart';
import 'package:infrastructure/src/movie/http/dto/movie_genre_dto.dart';
import 'package:infrastructure/src/movie/http/dto/movie_production_company_dto.dart';
import 'package:infrastructure/src/movie/http/dto/movie_simplified_dto.dart';
import 'package:infrastructure/src/movie/http/dto/movie_spoken_language_dto.dart';
import 'package:json_annotation/json_annotation.dart';

part 'movie_dto.g.dart';

@JsonSerializable()
class Movie extends MovieSimplified {
  Movie({
    required super.adult,
    required super.backdropPath,
    required this.belongsToCollection,
    required this.budget,
    required this.genres,
    required this.homepage,
    required super.id,
    required this.imdbId,
    required super.originalLanguage,
    required super.originalTitle,
    required super.overview,
    required super.popularity,
    required super.posterPath,
    required this.productionCompanies,
    required this.productionCountries,
    required super.releaseDate,
    required this.revenue,
    required this.runtime,
    required this.spokenLanguages,
    required this.status,
    required this.tagline,
    required super.title,
    required super.video,
    required super.voteAverage,
    required super.voteCount,
    required super.genreIds,
  });

  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);

  @JsonKey(name: 'belongs_to_collection', toJson: _toJsonBelongsToCollection)
  final BelongsToCollection? belongsToCollection;
  final int budget;
  @JsonKey(toJson: _toJsonGenre)
  final List<Genre> genres;
  final String homepage;
  @JsonKey(name: 'imdb_id')
  final String imdbId;
  @JsonKey(name: 'production_companies', toJson: _toJsonProductionCompanies)
  final List<ProductionCompany> productionCompanies;
  @JsonKey(name: 'production_countries', toJson: _toJsonProductionCountries)
  final List<ProductionCountry> productionCountries;
  final int revenue;
  final int runtime;
  @JsonKey(name: 'spoken_languages', toJson: _toJsonSpokenLanguage)
  final List<SpokenLanguage> spokenLanguages;
  final String status;
  final String tagline;

  @override
  Map<String, dynamic> toJson() => _$MovieToJson(this);

  //* Conversiones de forma automatica al Json, dentro de del json padre.
  static Map<String, dynamic>? _toJsonBelongsToCollection(
    BelongsToCollection? belongs,
  ) =>
      belongs?.toJson();

  static List<Map<String, dynamic>> _toJsonProductionCompanies(
    List<ProductionCompany> companies,
  ) =>
      companies.map((companie) => companie.toJson()).toList();

  static List<Map<String, dynamic>> _toJsonGenre(
    List<Genre> genres,
  ) =>
      genres.map((genre) => genre.toJson()).toList();

  static List<Map<String, dynamic>> _toJsonProductionCountries(
    List<ProductionCountry> countries,
  ) =>
      countries.map((countrie) => countrie.toJson()).toList();

  static List<Map<String, dynamic>> _toJsonSpokenLanguage(
    List<SpokenLanguage> languages,
  ) =>
      languages.map((language) => language.toJson()).toList();
}
