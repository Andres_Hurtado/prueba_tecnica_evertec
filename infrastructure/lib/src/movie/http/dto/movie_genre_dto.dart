
import 'package:json_annotation/json_annotation.dart';
part 'movie_genre_dto.g.dart';

@JsonSerializable()
class Genre {
  Genre({
    required this.id,
    required this.name,
  });
  factory Genre.fromJson(Map<String, dynamic> json) => _$GenreFromJson(json);

  final int id;
  final String? name;
  Map<String, dynamic> toJson() => _$GenreToJson(this);
}
