import 'package:envied/envied.dart';

part 'env.g.dart';

//* Usamos ofuscacion para nuestros claves privadas.
@Envied(obfuscate: true)
abstract class Env {
  @EnviedField(varName: 'API_KEY')
  static String apiKey = _Env.apiKey;
  @EnviedField(varName: 'BASE_URL')
  static String baseUrl = _Env.baseUrl;
  @EnviedField(varName: 'BASE_IMAGE_URL')
  static String baseImageUrl = _Env.baseImageUrl;
}
