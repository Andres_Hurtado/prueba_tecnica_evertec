import 'package:domain/domain.dart';
import 'package:infrastructure/src/movie/anticorruption/movie_simplified_translator.dart';
import 'package:infrastructure/src/movie/anticorruption/movie_translator.dart';
import 'package:infrastructure/src/movie/contracts/movie_local_repository.dart';
import 'package:infrastructure/src/movie/persistence/dao/movie_dao.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: MovieLocalRepository)
class MovieDriftRepository implements MovieLocalRepository {
  MovieDriftRepository({
    required MovieDao movieDao,
  }) : _movieDao = movieDao;

  final MovieDao _movieDao;

  @override
  Future<List<MovieSimplified>> getMovies(int page) async {
    final movies = await _movieDao.getMovieAll(page);
    return movies.map(MovieSimplifiedTranslator.fromDaoToDomain).toList();
  }

  @override
  Future<Movie?> getMovieById(int id) async {
    final movie = await _movieDao.getMovieById(id);
    if(movie == null) return null;
    return MovieTranslator.fromDaoToDomain(movie);
  }

  @override
  Future<void> insertMovies(List<MovieSimplified> movies) async {
    final movieEntity =
        movies.map(MovieSimplifiedTranslator.fromDomainToDao).toList();
    await _movieDao.insertMovies(movieEntity);
  }

  @override
  Future<void> updateById(Movie movie) async {
    final newMovieEntity = MovieTranslator.fromDomainToDao(movie);
    await _movieDao.updateMovie(newMovieEntity);
  }

  @override
  Future<bool> isEmpty() async => await _movieDao.getCountMovies() == 0;
}
