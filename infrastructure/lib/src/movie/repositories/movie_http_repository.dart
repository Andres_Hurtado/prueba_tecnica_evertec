import 'dart:developer';

import 'package:domain/domain.dart';
import 'package:infrastructure/infrastructure.dart';
import 'package:infrastructure/src/movie/anticorruption/movie_simplified_translator.dart';
import 'package:infrastructure/src/movie/anticorruption/movie_translator.dart';
import 'package:infrastructure/src/movie/contracts/movie_remote_repository.dart';
import 'package:infrastructure/src/movie/http/dto/movie_dto.dart' as dto_movie;
import 'package:infrastructure/src/movie/http/dto/movie_pagination_dto.dart'
    as dto_movie_pag;
import 'package:infrastructure/src/shared/http/http_client.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: MovieRemoteRepository)
class MovieHttpRepository implements MovieRemoteRepository {
  MovieHttpRepository({
    required HttpClient<dynamic, dynamic> httpClient,
  }) : _httpClient = httpClient {
    if (Env.apiKey.isEmpty && Env.baseUrl.isEmpty) {
      throw RequiredKeyException();
    }
  }

  final HttpClient<dynamic, dynamic> _httpClient;

  @override
  Future<List<MovieSimplified>> getMovies(int page) async {
    final queryParameters = {'page': page, 'language': 'es-ES'};
    const path = '/movie/popular';
    final token = Env.apiKey;
    log(Env.baseUrl + path);

    final movies = await _httpClient.request<List<MovieSimplified>, Object>(
      path,
      token: token,
      queryParameters: queryParameters,
      parser: parserPagination,
    );
    return movies;
  }

  @override
  Future<Movie> getById(int id) async {
    final queryParameters = {'language': 'es-ES'};
    final path = '/movie/$id';
    final token = Env.apiKey;

    final movies = await _httpClient.request<Movie, Object>(
      path,
      token: token,
      queryParameters: queryParameters,
      parser: parserMovie,
    );

    return movies;
  }

  @override
  Future<List<MovieSimplified>> searchMovie(
    int page, {
    required String textQuery,
  }) async {
    log('SEARCH');

    final queryParameters = {
      'page': page,
      'language': 'es-ES',
      'query': textQuery,
    };
    const path = '/search/movie';
    final token = Env.apiKey;

    final movies = await _httpClient.request<List<MovieSimplified>, Object>(
      path,
      token: token,
      queryParameters: queryParameters,
      parser: parserPagination,
    );
    return movies;
  }

  //* Metodo aparseador de data.
  List<MovieSimplified> parserPagination(Object data) {
    final movies =
        dto_movie_pag.MoviePagination.fromJson(data as Map<String, dynamic>);
    return movies.results
        .map(MovieSimplifiedTranslator.fromDtoToDomain)
        .toList();
  }

  //* Metodo aparseador de data.
  Movie parserMovie(Object data) {
    final results = dto_movie.Movie.fromJson(data as Map<String, dynamic>);
    return MovieTranslator.fromDtoToDomain(results);
  }
}
