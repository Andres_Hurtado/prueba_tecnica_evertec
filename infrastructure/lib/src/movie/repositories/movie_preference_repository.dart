import 'package:infrastructure/src/movie/contracts/movie_temporal_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

const _prefs = 'LastUpdated';

@Injectable(as: MovieTemporalRepository)
class MoviePreferenceTemporalRepository implements MovieTemporalRepository {
  @override
  Future<String> getLastUpdatedPreference() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(_prefs) ?? '';
  }

  @override
  Future<void> saveLastUpdatedPreference(String lastUpdated) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString(_prefs, lastUpdated);
  }
}
