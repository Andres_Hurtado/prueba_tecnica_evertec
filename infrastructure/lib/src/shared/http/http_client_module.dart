import 'package:dio/dio.dart';
import 'package:infrastructure/infrastructure.dart';
import 'package:injectable/injectable.dart';

@module
abstract class HttpClientModule {
  @LazySingleton()
  Dio dio() => Dio(BaseOptions(baseUrl: Env.baseUrl));
}
