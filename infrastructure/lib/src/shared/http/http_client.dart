// ignore_for_file: avoid_shadowing_type_parameters
import 'dart:developer';

import 'package:dio/dio.dart' as dio;
import 'package:infrastructure/src/shared/exceptions/http_client_exception.dart';
import 'package:injectable/injectable.dart';

@injectable
class HttpClient<T, S> {
  HttpClient({required dio.Dio client}) : _clientDio = client {
    _timeOut = const Duration(seconds: _maxSecondsToWaitResponse);
  }

  late final dio.Dio _clientDio;
  late final Duration _timeOut;
  late final String keyToken;
  static const _maxSecondsToWaitResponse = 10;

  Future<T> request<T, S>(
    String path, {
    required T Function(S data) parser,
    String method = 'GET',
    String? token,
    Map<String, dynamic>? queryParameters,
    Object? data,
    Map<String, String>? headers,
  }) async {
    try {
      headers = {'Authorization': 'Bearer $token', ...?headers};

      log(_clientDio.options.baseUrl+path);
      final response = await _clientDio
          .request<dynamic>(
            path,
            options: dio.Options(
              method: method,
              headers: headers,
              contentType: 'application/json',
            ),
            queryParameters: queryParameters,
            data: data,
          )
          .timeout(_timeOut);

      return parser(response.data! as S);
    } on dio.DioException catch (e) {
      if (e.response != null) {
        final data = e.response?.data as Map<String, dynamic>;
        log(data['status_message']?.toString() ?? '');
        throw HttpClientException(message: data['status_message']?.toString() ?? '');
      } else {
        log(e.toString());
        throw HttpClientException();
      }
    } catch (e) {
      log(e.toString());
      throw HttpClientException();
    }
  }
}
