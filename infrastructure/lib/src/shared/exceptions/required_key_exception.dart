import 'package:infrastructure/src/shared/exceptions/technical_exception.dart';

class RequiredKeyException extends TechnicalException {
  RequiredKeyException({String message = 'Se ha producido un error interno, datos requeridos en la petición.'})
      : super(message);
}
