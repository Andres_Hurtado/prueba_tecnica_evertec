import 'package:infrastructure/src/shared/exceptions/technical_exception.dart';

class HttpClientException extends TechnicalException {
  HttpClientException({String message = 'Se ha producido un error interno'})
      : super(message);
}
