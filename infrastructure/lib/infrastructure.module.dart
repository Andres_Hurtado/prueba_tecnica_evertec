//@GeneratedMicroModule;InfrastructurePackageModule;package:infrastructure/infrastructure.module.dart
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'dart:async' as _i2;

import 'package:dio/dio.dart' as _i3;
import 'package:domain/domain.dart' as _i15;
import 'package:infrastructure/src/movie/contracts/movie_local_repository.dart'
    as _i13;
import 'package:infrastructure/src/movie/contracts/movie_remote_repository.dart'
    as _i7;
import 'package:infrastructure/src/movie/contracts/movie_temporal_repository.dart'
    as _i9;
import 'package:infrastructure/src/movie/movie_proxy.dart' as _i16;
import 'package:infrastructure/src/movie/persistence/dao/movie_dao.dart'
    as _i12;
import 'package:infrastructure/src/movie/repositories/movie_drift_repository.dart'
    as _i14;
import 'package:infrastructure/src/movie/repositories/movie_http_repository.dart'
    as _i8;
import 'package:infrastructure/src/movie/repositories/movie_preference_repository.dart'
    as _i10;
import 'package:infrastructure/src/shared/http/http_client.dart' as _i4;
import 'package:infrastructure/src/shared/http/http_client_module.dart' as _i17;
import 'package:infrastructure/src/shared/network/network_module.dart' as _i18;
import 'package:infrastructure/src/shared/network/network_verify.dart' as _i11;
import 'package:infrastructure/src/shared/persistence/movie_database.dart'
    as _i6;
import 'package:injectable/injectable.dart' as _i1;
import 'package:internet_connection_checker/internet_connection_checker.dart'
    as _i5;

class InfrastructurePackageModule extends _i1.MicroPackageModule {
// initializes the registration of main-scope dependencies inside of GetIt
  @override
  _i2.FutureOr<void> init(_i1.GetItHelper gh) {
    final httpClientModule = _$HttpClientModule();
    final networkModule = _$NetworkModule();
    gh.lazySingleton<_i3.Dio>(() => httpClientModule.dio());
    gh.factory<_i4.HttpClient<dynamic, dynamic>>(
        () => _i4.HttpClient<dynamic, dynamic>(client: gh<_i3.Dio>()));
    gh.lazySingleton<_i5.InternetConnectionChecker>(
        () => networkModule.internetConnectionChecker());
    gh.singleton<_i6.MovieDatabase>(_i6.MovieDatabase());
    gh.factory<_i7.MovieRemoteRepository>(() => _i8.MovieHttpRepository(
        httpClient: gh<_i4.HttpClient<dynamic, dynamic>>()));
    gh.factory<_i9.MovieTemporalRepository>(
        () => _i10.MoviePreferenceTemporalRepository());
    gh.lazySingleton<_i11.NetworkVerify>(
        () => _i11.NetworkVerify(gh<_i5.InternetConnectionChecker>()));
    gh.factory<_i12.MovieDao>(() => _i12.MovieDao(gh<_i6.MovieDatabase>()));
    gh.factory<_i13.MovieLocalRepository>(
        () => _i14.MovieDriftRepository(movieDao: gh<_i12.MovieDao>()));
    gh.factory<_i15.MovieRepository>(() => _i16.MovieProxy(
          movieRemoteRepository: gh<_i7.MovieRemoteRepository>(),
          movieLocalRepository: gh<_i13.MovieLocalRepository>(),
          networkVerify: gh<_i11.NetworkVerify>(),
          movieTemporalRepository: gh<_i9.MovieTemporalRepository>(),
        ));
  }
}

class _$HttpClientModule extends _i17.HttpClientModule {}

class _$NetworkModule extends _i18.NetworkModule {}
